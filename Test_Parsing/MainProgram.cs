﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Test_Parsing.Entities;

namespace Test_Parsing
{
    class MainProgram
    {
        public void Start()
        {
            Car car = FindCar(); 
            Output(car); 
        }
        private Car FindCar()
        {
            ChromeDriver driver = new ChromeDriver();
            driver.Url = @"https://www.kellysubaru.com/used-inventory/index.htm";
            Thread.Sleep(3000);
            var cars = driver.FindElements(By.ClassName("inv-type-used"));
            var neededCar = cars[1];
            string url = neededCar.FindElement(By.XPath(".//img[@class='photo thumb']")).GetAttribute("src");
            string vin = neededCar.FindElement(By.XPath(".//dl[@class='vin']//dd")).Text;
            int price = Convert.ToInt32(neededCar.FindElement(By.XPath(".//li//span[@class='value']")).Text.Substring(1).Replace(",", "").Replace('.', ','));
            Car car = new Car(url, vin, price);
            return car; 
        }
        private void Output (Car car)
        {
            Console.WriteLine("VIN автомобиля:" + car.Vin);
            Console.WriteLine("Price (цена автомобиля):" + car.Price + "$");
            Console.WriteLine("Cover photo URL (ссылка на главную фотографию):" + car.Url);
        }
    }
}
