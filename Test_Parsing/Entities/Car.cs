﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Parsing.Entities
{
    public class Car
    {
        public string Url { get; set; }
        public string Vin { get; set; }
        public int Price { get; set; }
        public  Car(string url, string vin, int price)
        {
            Url = url;
            Vin = vin;
            Price = price; 
        }
    }
}
